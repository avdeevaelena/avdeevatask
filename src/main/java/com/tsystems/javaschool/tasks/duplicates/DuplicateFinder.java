package com.tsystems.javaschool.tasks.duplicates;

import java.io.File;
import java.io.*;
import java.util.*;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
   public boolean process(File sourceFile, File targetFile) {
        // TODO: Implement the logic here
        
        List <String> readList  = new ArrayList<String>();
         try  {
               Scanner sourse = new Scanner(sourceFile);
               while (sourse.hasNext()) 
                      readList.add(sourse.nextLine());
                      sourse.close();  
              }
        catch (Exception ex) {System.out.println(ex.getMessage());
                              return true;}

         
         Map <String, Integer> passMap = new HashMap<String, Integer> ();
         for (String str: readList)  {
              passMap.put(str, Collections.frequency(readList, str));
         }
         
        try {
        
        FileWriter target = new FileWriter(targetFile, false);
        for (Map.Entry <String, Integer> en  : passMap.entrySet()) 
              target.write(en.getKey()+" " + "[" +en.getValue()+ "]"+"\n" );  
              target.close();
            
        }
        catch  (Exception ex) {System.out.println(ex.getMessage());
                              return true;}
         
       return false;
    } 
    

    public static void main (String [] args ) {
     DuplicateFinder d = new DuplicateFinder(); 
    d.process(new File("C:\\Users\\admin\\Desktop\\javaschoolexam\\src\\test\\java\\com\\tsystems\\javaschool\\tasks\\duplicates\\a.txt"), new File("C:\\Users\\admin\\Desktop\\javaschoolexam\\src\\test\\java\\com\\tsystems\\javaschool\\tasks\\duplicates\\b.txt"));

    }
    
}
