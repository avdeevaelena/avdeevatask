
package com.tsystems.javaschool.tasks.calculator;


public class Control {
    
    protected  boolean isNumber (String value) {
    
      try { Double.parseDouble(value);
           return true;}
      catch (Exception ex) {return false;}      
    }
    
    protected  int isStage (String value) {
    switch (value) {
        case "-": return 1; 
        case "/": return 2;
        case "+": return 1;
        case "*": return 2;
        default: return 0;
       }
    }
    
    protected  boolean isSymbol(String value) {
       return value.equals("+") || value.equals("-") ||  value.equals("/") ||  value.equals("*"); 
    }
    
}
