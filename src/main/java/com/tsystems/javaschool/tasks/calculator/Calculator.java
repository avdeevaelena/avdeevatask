package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.LinkedList;
import java.math.*;



public class Calculator extends Control{

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        
        statement = statement.replaceAll("\\+"," + ").replaceAll("-"," - ").replaceAll("\\*"," * ").replaceAll("/"," / ").replaceAll("\\("," ( ").replaceAll("\\)"," ) ");
        
       
        LinkedList<String>  list = new LinkedList<>();
        Stack<String> stackValue = new Stack<>(); 
        
        for (String value: statement.split(" ")) {
             if (isNumber(value))  
                 list.add(value);
             else if ( "(".equals(value) || ( stackValue.empty()  && isSymbol(value))) 
                   stackValue.push(value); 
             else if (isSymbol(value)) {
                 while (! stackValue.empty() && isStage(value) <= isStage(stackValue.peek()))
                           list.add(stackValue.pop()); 
                           stackValue.push(value);
                }
             else if  ( ")".equals(value)){
                  if ("(".equals(stackValue.peek()) || !stackValue.contains("("))
                       return null;
                       while (!"(".equals(stackValue.peek())) 
                       list.add(stackValue.pop());
                       stackValue.pop();
                 } 
        }
         
      while (! stackValue.empty()) 
        list.add(stackValue.pop());
        if (list.contains("(") || list.contains(")")) 
           return null;
        
       Stack<Double> stackNumbers = new Stack<>();
       double num1;     
       double num2;
        for (String curval: list) {
            if (isNumber(curval)) stackNumbers.push(Double.parseDouble(curval));
            if (isSymbol(curval)) {
                        try { 
                            num2 = stackNumbers.pop();
                            num1 = stackNumbers.pop();
                      }
                      catch (Exception ex) {return null;}
              switch (curval){
                  case "*":  stackNumbers.push(num1*num2); break;
                  case "/":  stackNumbers.push(num1/num2); break;
                  case "-":  stackNumbers.push(num1-num2); break;
                  case "+":  stackNumbers.push(num1+num2); break;
                }    
            }  
        } 
 
       if (stackNumbers.size() !=1) 
            return null;
         
        
       double res =  stackNumbers.pop();
       String returnResStr = new BigDecimal(res).setScale(4, RoundingMode.HALF_EVEN).toString();
       if (returnResStr.charAt(returnResStr.length()-1) == '0'){
            int nextval = returnResStr.length()-1;
            char point;
            
            while ((point =returnResStr.charAt(nextval)) =='0'){
                 returnResStr = returnResStr.substring(0,returnResStr.length()-1);
                 nextval--;
            }
        }
        
       if (returnResStr.charAt(returnResStr.length()-1)=='.') 
           returnResStr = returnResStr.substring(0,returnResStr.length()-1);
            return returnResStr;   
   } 
} 
    


