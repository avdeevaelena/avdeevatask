package com.tsystems.javaschool.tasks.subsequence;

import com.tsystems.javaschool.tasks.calculator.Calculator;
import java.util.List;
import java.util.Iterator;
import java.util.Arrays;
public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
       
       Iterator  iter_x = x.listIterator();
       Iterator  iter_y = y.listIterator();
       
       if (x.size() > y.size()) return false;
       
       int i_x=0;
       int i_y=0;
       
       while (iter_x.hasNext()) {
           Object objectX = iter_x.next();
           i_x++;
           while(iter_y.hasNext()) {
             Object objectY = iter_y.next();
             if (objectX == objectY) {
                    i_y++;
                    break;
                }
            }
        if (i_y < i_x)  return false; 
        
       }
       
       if (i_y == x.size()) return true;
       else  return false;
    }
    
     public static void main(String[] args) {

      Subsequence s = new Subsequence();
       boolean b = s.find(Arrays.asList("A", "B", "C", "D"),
       Arrays.asList("BD", "A", "ABC", "B", "M", "D", "M", "C", "DC", "D"));
      System.out.println(b); // Result: true
    } 
    
}
