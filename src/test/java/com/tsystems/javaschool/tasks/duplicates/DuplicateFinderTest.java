package com.tsystems.javaschool.tasks.duplicates;

import org.junit.Test;

import java.io.File;

public class DuplicateFinderTest {

    private DuplicateFinder duplicateFinder = new DuplicateFinder();

    @Test(expected = IllegalArgumentException.class)
    public void test() {
        //run
        duplicateFinder.process(null, new File("C:\\Users\\admin\\Desktop\\javaschoolexam\\src\\test\\java\\com\\tsystems\\javaschool\\tasks\\duplicates\\a.txt"));

        //assert : exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void test1() {
        //run
        duplicateFinder.process(new File("C:\\Users\\admin\\Desktop\\javaschoolexam\\src\\test\\java\\com\\tsystems\\javaschool\\tasks\\duplicates\\a.txt"), null);

        //assert : exception
    }


}